# A* pathing

This is a small proof of concept of A* pathing in python using pygame. This allows the user to open a grid path and create obstacles for the path. The pathing algorithm will attempt to find the shortest path between the start and the end. It will error out if there is no path. Diagonals are taken into account

## Installing the requirements
If you have conda, it should be a simple matter of running:
```
conda env create
conda activate pypath
```
in your terminal and you'll have everything required to run it. Otherwise, have python 3.6 and pygame installed, using pip or otherwise.

## Running the application
Run the application with 
```
python main.py
```