import pypath
from grid import GameGrid


def main():
    gridsize = (100, 100)
    start =  (1,1)
    end = (0, 90)
    print('starting')
    
    myGrid = GameGrid(100, 100, start, end)
    myGrid.begin()

if __name__ == '__main__':
    main()