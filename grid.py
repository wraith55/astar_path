import pygame
import sys
import pypath

BLACK = (0, 0, 0)
WHITE = (255, 255, 255)
GREEN = (0, 255, 0)
RED = (255, 0, 0)
YELLOW = (255, 255, 0)
BLUE = (0, 0, 255)

class GameGrid:
    
    MARGIN = 1
    WINDOW_WIDTH = 1000
    WINDOW_HEIGHT = 1000
    WINDOW_SIZE = [WINDOW_WIDTH, WINDOW_HEIGHT+50]
    def __init__(self, width, height, start, goal):
        self.width = width
        self.height = height
        self.start = start
        self.goal = goal
        pygame.init()
        self.screen = pygame.display.set_mode(self.WINDOW_SIZE)
        pygame.display.set_caption("pypath")

        self.done = False

        self.clock = pygame.time.Clock()
        self.initGrid()
        
        
        

    def begin(self):
        while True:
            #self.updateGrid()
            for event in pygame.event.get():
                if event.type == pygame.QUIT:
                    pygame.quit()
                    sys.exit()
                elif event.type == pygame.MOUSEBUTTONDOWN:
                    self.handleMouseDown(event)
                pygame.display.update()


    def initGrid(self):
        '''
        starts up the grid
        '''
        self.screen.fill(BLACK)
        (startx, starty) = self.start
        (goalx, goaly) = self.goal

        


        self.blockSize = int(min(self.WINDOW_WIDTH // self.width, self.WINDOW_HEIGHT // self.height))
        self.grid = []
        for y in range(0, self.WINDOW_HEIGHT, self.blockSize):
            gridrow = []
            start_pos = (0, y)
            end_pos = (self.WINDOW_WIDTH, y)
            pygame.draw.line(self.screen, WHITE, start_pos, end_pos, 1)
            for x in range(0, self.WINDOW_WIDTH, self.blockSize):
                gridrow.append(0)
            self.grid.append(gridrow)
        for x in range(0, self.WINDOW_WIDTH, self.blockSize):
            start_pos = (x, 0)
            end_pos = (x, self.WINDOW_HEIGHT)
            pygame.draw.line(self.screen, WHITE, start_pos, end_pos, 1)
        self.grid[starty][startx] = 2
        self.grid[goaly][goalx] = 3
        self.updateGrid()
        self.drawStartButton()
        self.drawResetButton()
        
    def updateGrid(self):
        for y in range(0, self.WINDOW_HEIGHT, self.blockSize):
            for x in range(0, self.WINDOW_WIDTH, self.blockSize):
                gridVal = self.grid[int(y // self.blockSize)][int(x // self.blockSize)]
                if gridVal == 1:
                    self.drawRect(WHITE, x, y)
                elif gridVal == 2:
                    self.drawRect(BLUE, x, y)
                elif gridVal == 3:
                    self.drawRect(RED, x, y)
                elif gridVal == 4:
                    self.drawRect(GREEN, x, y)
    

    def getPath(self):
        self.path = pypath.aStar(self.start, self.goal, (self.width, self.height), self.getRestrictedPoints())
        self.drawPath()

    def drawPath(self):
        '''
        draw the path astar found on the grid
        '''
        for point in self.path:
            (pointx, pointy) = point
            self.grid[pointy][pointx] = 4
        self.updateGrid()

    def resetGrid(self):
        '''
        resets the grid to remove user defined blocks and any paths already found
        '''
        self.path = []
        self.initGrid()
        self.updateGrid()

    def drawRect(self, color, x, y):
        '''
        draw a rectangle at given coords with given color
        '''
        rect = pygame.Rect(x, y, self.blockSize, self.blockSize)
        pygame.draw.rect(self.screen, color, rect)
    
    def drawStartButton(self):
        self.startButton = pygame.Rect(0, self.WINDOW_HEIGHT, 100, 50)
        pygame.draw.rect(self.screen, WHITE, self.startButton)

        font = pygame.font.SysFont('Arial', 25)
        self.screen.blit(font.render('Start', True, BLACK), (1, self.WINDOW_HEIGHT))

    def drawResetButton(self):
        self.resetButton = pygame.Rect(110, self.WINDOW_HEIGHT, 100, 50)
        pygame.draw.rect(self.screen, WHITE, self.resetButton)

        font = pygame.font.SysFont('Arial', 25)
        self.screen.blit(font.render('Reset', True, BLACK), (110, self.WINDOW_HEIGHT))

    def handleMouseDown(self, event):
        '''
        handles when the user clicks a box
        '''
        pos = event.pos
        (posx, posy) = pos
        if 0 < posx < self.WINDOW_WIDTH and 0 < posy < self.WINDOW_HEIGHT:
            gridx = int(posx // self.blockSize)
            gridy = int(posy // self.blockSize)
            self.grid[gridy][gridx] = 1
            self.updateGrid()
        elif self.startButton.collidepoint(pos):
            self.getPath()
        elif self.resetButton.collidepoint(pos):
            self.resetGrid()

    def getRestrictedPoints(self):
        '''
        gets the points the user has selected to be an obstacle
        '''
        restrictedPoints = []
        for y in range(len(self.grid)):
            gridrow = self.grid[y]
            for x in range(len(gridrow)):
                if gridrow[x] == 1:
                    restrictedPoints.append((x, y))
        return restrictedPoints
            

        
