import heapq
from collections import defaultdict
import sys
import math


def gridprint(gridsize, start, end, path=None):
    (width, height) = gridsize
    chararr = [['.' for x in range(width)] for y in range(height)]
    (startx, starty) = start
    (endx, endy) = end
    chararr[starty][startx] = '*'
    chararr[endy][endx] = '*'
    if path is not None:
        for point in path:
            (pointx, pointy) = point
            chararr[pointy][pointx] = '='
    print_char_arr(chararr)

def print_char_arr(chararr):
    print('\n')
    for x in chararr:
        print("".join(x))
    
def dist(pos1, pos2):
    (x1, y1) = pos1
    (x2, y2) = pos2
    # distance formula
    return math.sqrt((x2 - x1) ** 2 + (y2 - y1) ** 2) 


def reconstructPath(camefrom, current):
    totalpath = [current]
    while current in camefrom.keys():
        current = camefrom[current]
        totalpath = [current] + totalpath
    return totalpath


def aStar(start, goal, gridsize, restrictedPoints=[]):
    '''
    start: first point
    end: destination point
    grid: width and height of total grid size
    '''

    (width, height) = gridsize
    camefrom = {}

    gscore = defaultdict(lambda: sys.maxsize )
    gscore[start] = 0
    
    fscore = defaultdict(lambda: sys.maxsize )
    fscore[start] = dist(start, goal)

    openset = []
    heapq.heappush(openset, (fscore[start], start))


    while openset:
        (_, current) = heapq.heappop(openset)
        if current == goal:
            print('goal reached!')
            return reconstructPath(camefrom, current)
        for neighbor in getNeighbors(current, width, height, restrictedPoints):
            tentative_gscore = gscore[current] + dist(current, neighbor)
            if tentative_gscore < gscore[neighbor]:
                camefrom[neighbor] = current
                gscore[neighbor] = tentative_gscore
                fscore[neighbor] = gscore[neighbor] + dist(neighbor, goal)
                if not is_in_queue(neighbor, openset):
                    heapq.heappush(openset, (fscore[neighbor], neighbor))
                else:
                    update_value_in_queue(neighbor, openset, fscore[neighbor])



def getNeighbors(pos, width, height, restrictedPoints):
    '''
    get all neighbors from a current position
    '''
    neighbors = []
    (posx, posy) = pos
    for x in range(-1,2):
        for y in range(-1,2):
            if not isRestricted(posx + x, posy + y, width, height, restrictedPoints) and not (x == 0 and y == 0):
                neighbors.append((posx + x, posy + y))
    return neighbors
            

def isRestricted(posx, posy, width, height, restrictedPoints):
    '''
    checks if a point is not allowed
    '''
    if not 0 <= posx <= width:
        return True
    if not 0 <= posy <= height:
        return True
    pos = (posx, posy)
    if pos in restrictedPoints:
        return True
    return False

def is_in_queue(x, q):
    '''
    checks to see if value is in a queue already
    '''
    for (_, q_key) in q:
        if x == q_key:
            return True
    return False

def update_value_in_queue(x, q, new_val):
    '''
    updates a value in the queue after finding it
    '''
    for i in range(len(q)):
        (q_val, q_key) = q[i]
        if x == q_key:
            q_val = new_val
            q[i] = (q_val, q_key)
            heapq.heapify(q)
            return q
    raise KeyError('key not present')
